
#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

TCP_SEQ=$(tshark -nr $1 -Y "tcp.analysis.flags" -T fields -e tcp.stream | sort -n | uniq -c | sort -nr | head -1 |cut -f2 -d" ")

echo "Most common TCP stream: $TCP_SEQ"
tshark -nr $1 -q -z conv,tcp,"tcp.stream == $TCP_SEQ" |tee $0-tmp
IP1=$(grep "<->" $0-tmp | awk '{print $1; exit}'| sed 's/:.*$//g')
IP2=$(grep "<->" $0-tmp | awk '{print $3; exit}'| sed 's/:.*$//g')

echo "$IP1 ` whois n $IP1 |grep OrgName`"
echo "$IP2 ` whois n $IP2 |grep OrgName`"
rm $0-tmp

NAME=$(echo $1|sed s/\.pcap$//g)

echo "Isolating stream to file: $NAME-stream-$TCP_SEQ.pcap"
if [ -f "$NAME-stream-$TCP_SEQ.pcap" ]; then
    echo "file already exists, continuing without writing..."
    exit 0
fi


tshark -nr $1 -w $NAME-stream-$TCP_SEQ.pcap -Y "tcp.stream == $TCP_SEQ"
exit 0
