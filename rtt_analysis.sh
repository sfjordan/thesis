#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi 

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

RET=0

echo "RTT Analysis (secs):"
tshark -nr $1 -Y "tcp.analysis.ack_rtt" -e tcp.analysis.ack_rtt -T fields | perl stats.pl > $0-tmp; RET=$?
cat $0-tmp | sed '/^\s*$/d'
rm $0-tmp

exit $RET
