#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

capinfos $1 >$0-tmp
grep "byte rate" $0-tmp
grep "packet rate" $0-tmp
grep "Data size" $0-tmp
grep "duration" $0-tmp
rm $0-tmp

exit 0
