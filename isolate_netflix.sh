#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

NTFLX_SEL_1="ip.addr == 23.246.0.0/18 or ip.addr == 37.77.184.0/22 or ip.addr == 37.77.188.0/23 or ip.addr == 37.77.190.0/23 or ip.addr == 38.68.192.120/30 or ip.addr == 38.72.126.0/24 or ip.addr == 38.88.54.72/29 or ip.addr == 38.88.189.216/29 or ip.addr == 38.103.77.152/31 or ip.addr == 38.103.77.170/30 or ip.addr == 38.103.77.254/31 or ip.addr == 38.103.78.52/31 or ip.addr == 38.104.94.32/30 or ip.addr == 38.104.94.52/30 or ip.addr == 38.104.95.148/30 or ip.addr == 38.104.115.188/30 or ip.addr == 38.104.127.36/28 or ip.addr == 38.104.127.56/30 or ip.addr == 38.104.127.64/29 or ip.addr == 38.104.127.76/30"
NTFLX_SEL_2="ip.addr == 64.120.128.0/17 or ip.addr == 66.197.128.0/17 or ip.addr == 67.94.112.0/29 or ip.addr == 69.53.224.0/19 or ip.addr == 108.175.32.0/20 or ip.addr == 149.6.58.64/30 or ip.addr == 149.6.98.132/30 or ip.addr == 149.6.98.144/30 or ip.addr == 149.6.165.80/30 or ip.addr == 185.2.220.0/23 or ip.addr == 185.2.222.0/23 or ip.addr == 185.9.188.0/22 or ip.addr == 192.173.64.0/18 or ip.addr == 198.38.96.0/19 or ip.addr == 198.45.48.0/20 or ip.addr == 200.175.6.84/30 or ip.addr == 207.45.72.0/22 or ip.addr == 208.75.76.0/22 or ip.addr == 216.1.124.0/29 or ip.addr == 216.156.192.0/29"
NTFLX_SEL_3="ip.addr == 38.104.139.204/30 or ip.addr == 38.122.38.120/29 or ip.addr == 38.122.38.140/30 or ip.addr == 38.122.38.176/30 or ip.addr == 38.122.38.180/29 or ip.addr == 38.122.38.188/28 or ip.addr == 38.122.39.36/30 or ip.addr == 38.122.39.72/30 or ip.addr == 38.122.47.72/30 or ip.addr == 38.122.47.144/29 or ip.addr == 38.122.62.128/30 or ip.addr == 38.122.63.44/30 or ip.addr == 38.122.63.220/30 or ip.addr == 38.122.147.80/30 or ip.addr == 38.122.147.104/30 or ip.addr == 38.122.181.92/30 or ip.addr == 38.122.181.220/30 or ip.addr == 38.122.181.228/30 or ip.addr == 38.122.228.64/30 or ip.addr == 38.122.228.68/29 or ip.addr == 38.122.228.160/30 or ip.addr == 38.122.228.224/30 or ip.addr == 45.57.0.0/17"
NTFLX_SEL="$NTFLX_SEL_1 or $NTFLX_SEL_2 or $NTFLX_SEL_3"


NAME=$(echo $1|sed s/\.pcap$//g)

echo "Isolating Netflix data to file: $NAME-netflix.pcap"
if [ -f "$NAME-netflix.pcap" ]; then
    echo "file already exists, continuing without writing..."
    exit 0
fi

tshark -nr $1 -w $NAME-netflix.pcap -Y "$NTFLX_SEL"
exit 0