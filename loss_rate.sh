
#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

NUM_PACKETS=$(tshark -nr $1 | wc -l)

#lost: tcp.analysis.retransmission or tcp.analysis.fast_retransmission or tcp.analysis.zero_window or tcp.analysis.lost_segment or tcp.analysis.out_of_order or tcp.analysis.duplicate_ack_frame
NUM_LOST=$(tshark -nr $1 -Y "tcp.analysis.lost_segment" | wc -l)

echo "num packets: $NUM_PACKETS"
echo "num lost: $NUM_LOST"

printf "Packet loss rate: %06.3f %%\n" $(bc -l <<< scale=4\;$NUM_LOST*100/$NUM_PACKETS)
exit 0

