#!/bin/bash

if [ $# -ne 2 ]; then
    echo "USAGE: $0 PCAP_FILENAME INTERVAL(sec)"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

INT_RE='^[0-9]+$'
if ! [[ $2 =~ $INT_RE ]] ; then
   echo "USAGE: $2 must be an integer"
   exit 1
fi

tshark -nr $1 -q -z  io,stat,$2,"COUNT(tcp.analysis.retransmission)tcp.analysis.retransmission","AVG(tcp.window_size)tcp.window_size","MAX(tcp.window_size)","MIN(tcp.window_size)tcp.window_size"
