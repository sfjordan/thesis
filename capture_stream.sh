#!/bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
COLOREND='\033[0m'

LOC="/Volumes/Data/stream_captures"
# LOC="/Users/sjordan/Downloads"

cleanup() {
# example cleanup function
	echo "Cleaing up (this could take a while)"
	tshark -nr "$LOC/$NAME-tmp.pcap" -Y "ip.addr == $IP" -w "$LOC/$NAME-$(date '+%m-%d_%H-%M').pcap"
	RES=$?
	rm "$LOC/$NAME-tmp.pcap"
	return $RES
}

control_c() {
# run if user hits control-c
	echo -e "\n${RED}Stopping capture${COLOREND}...\n"
	#stop tshark process
	kill -INT $TSHARK_PID
	cleanup
	exit $?
}


echo -ne "Name of file?\n\t"
read NAME

echo -ne "IP address of device?\n\t"
read IP

if [[ $IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
	echo "previewing packets to that IP, ctrl-c continues to capture stage..."
	tshark -i en0 -I -Y "ip.addr == $IP"
	bash network_test.sh |tee "$LOC/$NAME.txt"
else
	echo "Invalid IP address!"
	echo "exiting..."
	exit 1
fi

# trap keyboard interrupt (control-c)
trap control_c SIGINT

echo -ne "\n${GREEN}Starting Capture${COLOREND}...\n"
tshark -i en0 -I -w "$LOC/$NAME-tmp.pcap" &
TSHARK_PID=$!
wait $TSHARK_PID