#!/bin/bash

echo "Testing network speed..."
echo ""
python speedtest-cli | grep -E "Download|Upload"
echo ""
echo "Testing network loss/latency..."
echo ""
ping -c 50 8.8.8.8 >network_test-temp 2>&1
python -c '
import sys
f=open("network_test-temp","r")
lines = f.readlines()
f.close()
for line in lines:
	if (line.find("packet") != -1):
		packetline = line
packetloss = float(packetline.split(",")[-1].rstrip()[1:].split("%")[0])
if (packetloss != 100.0):
	avgtime = float(lines[-1].split("/")[-3])
else:
	avgtime = "--"

print "Packet loss: "+str(packetloss)+"%"
print "Latency: "+str(avgtime)+"ms"
'
echo ""

rm "network_test-temp"