#!/bin/bash

GREEN='\033[0;32m'
BLUE='\033[0;34m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
COLOREND='\033[0m'

function check_exit {
    local status=$?
    if [ $status -ne 0 ]; then
        echo -e "${RED}FAIL${COLOREND}\n"
    else
        echo -e "${GREEN}OK${COLOREND}\n"
    fi
}

if [ $# -ne 1 ]; then
    echo "USAGE: $0 PCAP_FILENAME"
    exit 1
fi

if [[ $1 != *.pcap ]]; then
    echo "USAGE: $1 must be *.pcap file"
    exit 1
fi

echo -ne "Print verbose tables (y/N)?\n\t"
read VERBOSE

if [[ $VERBOSE == "y" ]]; then
    echo -ne "Desired interval (seconds):\n\t"
    read INTERVAL
else
    VERBOSE="n"
fi


echo -ne "\n*** ISOLATING NETFLIX DATA ***\n\n"
bash isolate_netflix.sh $1 |tee $0-tmp

check_exit

NTFLX_FILE=$(grep 'data to file' $0-tmp | awk '{print $6; exit}')
rm $0-tmp

# echo -ne "\n*** ISOLATING TCP STREAM ***\n"
# bash isolate_biggest_tcp.sh $1 |tee $0-tmp
# check_exit
# TCP_FILE=$(grep 'stream to file' $0-tmp | awk '{print $5; exit}')
# rm $0-tmp

echo -ne "\n*** INFO OF TCP STREAM ***\n\n"
bash pcap_info.sh $NTFLX_FILE
check_exit
#echo -e "---\n"

bash loss_rate.sh $NTFLX_FILE
check_exit
if [[ $VERBOSE == "y" ]]; then
    bash verbose_loss_evolution.sh $NTFLX_FILE $INTERVAL
    check_exit
fi
#echo -e "---\n"

bash rtt_analysis.sh $NTFLX_FILE
check_exit
if [[ $VERBOSE == "y" ]]; then
    bash verbose_rtt_evolution.sh $NTFLX_FILE $INTERVAL
    check_exit
fi
#echo -e "---\n"

bash window_analysis.sh $NTFLX_FILE
check_exit
if [[ $VERBOSE == "y" ]]; then
    bash verbose_window_evolution.sh $NTFLX_FILE $INTERVAL
    check_exit
fi


echo -ne "\n*** DONE ***\n"
exit 0
